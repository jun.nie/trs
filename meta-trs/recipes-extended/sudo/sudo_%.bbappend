# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://admin_group.in"

DEPENDS += "gettext-native"

do_install:append() {

    export ADMIN_GROUP="sudo"
    export ADMIN_GROUP_OPTIONS="NOPASSWD:"

    envsubst < "${WORKDIR}/admin_group.in" > "${D}${sysconfdir}/sudoers.d/admin_group"
    chmod 644 "${D}${sysconfdir}/sudoers.d/admin_group"
}
