#
# SPDX-License-Identifier: MIT
#

import os

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.runtime.decorator.package import OEHasPackage
from oeqa.core.decorator.oetimeout import OETimeout

class OpteeTestSuite(OERuntimeTestCase):
    """
    Run OP-TEE tests (xtest).
    """
    @OETimeout(800)
    @OEHasPackage(['optee-test'])
    def test_opteetest_xtest(self):
        # clear storage before executing tests
        cmd = " xtest --clear-storage || true "
        status, output = self.target.run(cmd, timeout=60)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        # these tests seem to trigger a hang in qemu machine when tests
        # run under oeqa runtime test framework
        cmd = " xtest \
                    -x regression_6020 \
                    -x regression_8101 \
                    -x pkcs11_1009 \
              "
        status, output = self.target.run(cmd, timeout=600)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
