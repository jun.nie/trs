#!/bin/bash
#
# This script generates an image that can be booted in the Arm Virtual Hardware (AVH) infrastructure
# https://avh.arm.com/
#
# Initially copied from https://developer.arm.com/documentation/107660/0200/Custom-Firmware/Firmware/Package-Ubuntu-Server-Firmware-for-AVH?lang=en
# and adapted for TRS
#

TRS=$(pwd)
# FIXME: do not hardcode the kernel version
IN_DT=${TRS}/build/tmp_trs-qemuarm64/work/trs_qemuarm64-trs-linux/linux-yocto/6.4.14+git/linux-trs_qemuarm64-standard-build/arch/arm64/boot/dts/freescale/imx93-11x11-evk.dtb
IN_WIC=${TRS}/build/tmp_trs-qemuarm64/deploy/images/trs-qemuarm64/trs-image-trs-qemuarm64.rootfs.wic
RESIZE2FS=${TRS}/build/tmp_trs-qemuarm64/sysroots-components/x86_64/e2fsprogs-native/sbin/resize2fs
ZIP=${TRS}/build/tmp_trs-qemuarm64/sysroots-components/x86_64/zip-native/usr/bin/zip

function usage {
	echo "Usage: $0 [DT_file.dtb [image.wic]]"
	echo
	echo "Make a TRS image for the AVH (Arm Virtual Hardware) service."
	echo "This script should be run from the top-level directory once the TRS root FS is built."
	echo "For example:"
	echo
	echo "  trs-workspace$ make trs"
	echo "  trs-workspace$ $0"
	echo
	echo "Default DT file: $IN_DT"
	echo "Default image file: $IN_WIC"
	echo "The output is generated in a temporary directory shown when the script is run."
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
	usage
	exit 0
fi

[ "$1" ] && IN_DT="$1"
[ "$2" ] && IN_WIC="$2"

function cleanup {
	STATUS=$?
	if [ "${LO}" != "" ]; then
		sudo losetup -d "${LO}"
	fi
	if [ "${STATUS}" -ne 0 ] && [ -e "${WD}/log.txt" ] ; then
		echo "An error occurred. Content of ${WD}/log.txt:" >&3
		echo "========" >&3
		cat ${WD}/log.txt >&3
		echo "========" >&3
	fi
}

exec 3>&1
trap cleanup EXIT

if [ ! -e Makefile ] || [ ! -d build ] || [ ! -d trs ] ; then
	echo "This script needs to be run from the top of the TRS build directory"
	echo "and after a successful build (make trs)"
	exit 1
fi

set -e

WD=$(mktemp -d)
echo "Work dir: ${WD}"
pushd ${WD} >/dev/null
{
	set -v
	echo "Copying TRS image to work directory..." >&3
	cp ${IN_WIC} nand
	echo "Extending TRS image size to 10G..." >&3
	# Make root FS bigger. First extend image file.
	truncate -s 10G nand
	# Attach the image file
	LO="$(losetup -f)"
	sudo losetup -P "${LO}" nand
	echo "Downloading 'growpart'..." >&3
	wget -O growpart https://github.com/canonical/cloud-utils/raw/main/bin/growpart
	chmod +x growpart
	echo "Extending root partition size..." >&3
	# Make partition 2 fill the whole file
	sudo ./growpart "${LO}" 2
	echo "Resizing root FS..." >&3
	# Need version >= 1.47.0
	sudo ${RESIZE2FS} "${LO}p2"

	# Mount partition 1 to directory boot
	mkdir boot
	sudo mount "${LO}p1" boot
	echo "Extracting kernel from TRS image..." >&3
	# Copy the kernel
	cp boot/Image kernel
	sudo umount boot
	rm -r boot/

	echo "Copying device tree to work directory..." >&3
	cp ${IN_DT} devicetree

	echo "Creating Info.plist..." >&3
	# Create the Info.plist file
	# FIXME: extract actual version  from the TRS image
	VERSION=0.3.0
	BUILD=$(md5sum image/nand | cut -c 1-8)
	cat << EOF > Info.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>Type</key>
<string>iot</string>
<key>UniqueIdentifier</key>
<string>TRS on i.MX93</string>
<key>DeviceIdentifier</key>
<string>imx93</string>
<key>Version</key>
<string>${VERSION}</string>
<key>Build</key>
<string>${BUILD}</string>
</dict>
</plist>
EOF
	echo "Creating ZIP file..." >&3
	${ZIP} trs.zip Info.plist nand devicetree kernel
	set +v
} >${WD}/log.txt 2>&1
popd >/dev/null
echo "AVH image file written to: ${WD}/trs.zip"
