..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

##########
User Guide
##########

.. toctree::
   :maxdepth: 1
   :caption: Initial setup

   prerequisites

Covers the steps necessary to configure your host system to develop and deploy
TRS onto supported devices.

.. toctree::
   :maxdepth: 1
   :caption: Device specific setup

   devices

Describes how to work with various devices.

.. toctree::
   :maxdepth: 1
   :caption: Customizations for unsupported targets

   extend

Describes how TRS may be customized and run on unsupported target platforms.
