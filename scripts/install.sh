#!/usr/bin/env bash
PYTHON3=$(which python3)

################################################################################
# Configuration
################################################################################
if [[ -z "${PYVENV_PATH}" ]]; then
	echo "Setting up locally"
	PYVENV_PATH=.pyvenv
fi

APT_PKGS=$(tr '\n' ' ' <apt-packages.txt)

PYTHON_PKGS="
	sphinxcontrib-plantuml \
	sphinxcontrib-programoutput \
	sphinx-tabs \
	sphinx_rtd_theme \
	pep8
	"

################################################################################
# Main
################################################################################
VIRTUAL=

install_apt() {
	echo "To build 'TRS', the following (apt) packages needs to be installed:"
	if [[ -z "${VIRTUAL}" ]]; then
		APT_PKGS="${APT_PKGS}"
	fi
	echo "  $(echo ${APT_PKGS})"
	while true; do
		read -p "sudo access is required, continue? [y/n] " yn
		case $yn in
		[Yy]*)
			sudo apt update && sudo apt install -y ${APT_PKGS}
			break
			;;
		[Nn]*) break ;;
		*) echo "Please answer 'y' or 'n'." ;;
		esac
	done
}

install_pip() {
	if [[ -z "${PYTHON3}" ]]; then
		echo "ERROR: python3 not installed"
		exit
	fi

	if [[ "${VIRTUAL}" == "y" ]]; then
		PYTHON_PKGS="${PYTHON_PKGS} virtualenv"
		if [[ -d ${PYVENV_PATH} ]]; then
			echo "${PYVENV_PATH} exist, reusing the environment"
			source ${PYVENV_PATH}/bin/activate
		else
			echo "Setting up virtual environment in ${PYVENV_PATH}"
			echo "${PYTHON3}"
			${PYTHON3} -m venv ${PYVENV_PATH}
			source ${PYVENV_PATH}/bin/activate
		fi
	else
		echo "Virtual environment NOT chosen, pip packages will be installed to the host system."
		IFS=
		read -r -p "Waiting 10 seconds, press any key to abort ... " -t 10 -n 1 -s stop
		if [[ ! -z "${stop}" ]] || [[ ${stop} == "" ]]; then
			exit
		fi
	fi
	pip install ${PYTHON_PKGS}
	pip list
}

help() {
	echo "Usage:"
	echo "  install.sh -h           Display this help message"
	echo "  install.sh -a           Install APT packages"
	echo "  install.sh -p           Install Python packages"
	echo "  install.sh -v           Setup a virtual Python environment"
}

while getopts "hvap" flag; do
	case "${flag}" in
	h)
		help
		exit
		;;
	v) VIRTUAL=y ;;
	a) INSTALL_APT=y ;;
	p) INSTALL_PIP=y ;;
	\?)
		echo "ERROR: Unknown option provided"
		help
		exit
		;;
	esac
done

if [[ ! -z "${INSTALL_APT}" ]]; then
	install_apt
fi

if [[ ! -z "${INSTALL_PIP}" ]]; then
	install_pip
fi
